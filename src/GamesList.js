import React from 'react';
import GameCard from './GameCard';

export default function GamesList({ games }) {
	console.log('====> GAMES: ', games);
	const emptyMessage = <p>There are no games yet in your collection.</p>;

	const gamesList = (
    <div className="ui four cards">
      { games.map(game => <GameCard game={game} key={game._id} />) }
    </div> 
  );

	return (
		<div>
			{games.length === 0 ? emptyMessage : gamesList}
		</div>
	);
}
