import React, { Component } from 'react';
import {Route, NavLink } from 'react-router-dom';
import GamesPage from './GamesPage';
import GameForm from './GameForm';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="ui container">
        <div className="ui menu">
          <div className="header item">Brand</div>
          <NavLink exact to='/' className='item'>Home</NavLink>
          <NavLink exact to='/games' className='item'>Games</NavLink>
          <NavLink exact to='/games/new' className='item right'>Add New Game</NavLink>
        </div>

        <Route exact path="/games" component={GamesPage} />
        <Route exact path="/games/new" component={GameForm} />
      </div>
    );
  }
}

export default App;
