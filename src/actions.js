export const SET_GAMES = 'SET_GAMES';
export const ADD_GAME = 'ADD_GAME';

// Temporarily add handleResponse function here
function handleResponse(response) {
  // meaning status code 200 or something
  if (response.ok) {
    return response.json();
  } else {
    let error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

// Set default games object
export function setGames(games) {
  return {
    type: SET_GAMES,
    games
  }
}

// Create thunk action: Fetch game
export function fetchGames() {
  return dispatch => {
    // returns promise
    fetch('/api/games')
      .then(res => res.json())
      .then(data => dispatch(setGames(data.games)));
  }
}

export function addGame(game) {
  return {
    type: ADD_GAME,
    game
  }
}

// Create thunk action: Save game
export function saveGame(data) {
  return dispatch => {
    return fetch('/api/games', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    // chain with promise handleResponse
  }).then(handleResponse)
  .then(data => dispatch(addGame(data.game)));
  }
}