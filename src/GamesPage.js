import React, { Component } from 'react';
import GamesList from './GamesList';
import { connect } from 'react-redux';
import {fetchGames} from './actions';

class GamesPage extends Component {
    componentDidMount() {
        this.props.fetchGames();
    }

    render() {
        return (
            <div>
                <h1>Games List</h1>

                <GamesList games={this.props.games} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        games: state.games
    }
}

// Takes some state from redux store and pass to props
export default connect(mapStateToProps, { fetchGames })(GamesPage);
