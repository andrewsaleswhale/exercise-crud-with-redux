import express from 'express';
import mongodb from 'mongodb';
import bodyParser from 'body-parser';

const app = express();
app.use(bodyParser.json());
const dbUrl = 'mongodb://localhost/crudwithredux';

function validate(data) {
  let errors = {};
  if (data.title === '') errors.title = "Can't be empty";
  if (data.cover === '') errors.cover = "Can't be empty";
  const isValid = Object.keys(errors).length === 0;
  return { errors, isValid }
}

mongodb.MongoClient.connect(dbUrl, function(err, db) {

  app.get('/api/games', (req, res) => {
    db.collection('games').find({}).toArray((err, games) => {
      res.json({ games });
    });
  });

  app.post('/api/games', (req, res) => {
    const { errors, isValid } = validate(req.body);
    if (isValid) {
      // save to database (mongodb)
      // console.log("===> req.body: ", req.body);
      const { title, cover } = req.body;
      db.collection('games').insert({ title, cover }, (err, result) => {
        // In case of database error, send a global message
        if (err) {
          res.status(500).json({ errors: {global: "Something went wrong"}});
        // In case of success
        } else {
          res.json({ game: result.ops[0]});
        }
      });
    } else { 
      res.status(400).json({ errors });
    }
  });

  app.use((req, res) => {
    res.status(404).json({
      errors: {
        global: "Something went wrong. Please try again later."
      }
    });
  });

  app.listen(8080, () => console.log('Server is running on localhost:8080'));

});
